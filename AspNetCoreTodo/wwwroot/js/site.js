﻿// Write your JavaScript code.

$(document).ready(function () {
    $('#add-item-button').on('click', addItem);
    $('.done-checkbox').on('click', function(e){
        markCompleted(e.target);
    });
});

function addItem() {
    var errorDiv = $('add-item-error');
    errorDiv.hide();
    var newTitle = $('#add-item-title').val();

    $.post('/Todo/AddItem', { title: newTitle }, function () {
        window.location = '/Todo';
    })
    .fail(function (data) {
        if (data && data.responseJSON) {
            var respJson = data.responseJSON;
            var firstError = respJson[Object.keys(respJson)[0]];
            $('#add-item-error').text(firstError);
            errorDiv.text(firstError);
            errorDiv.show();
        }
    });
}

function markCompleted(checkbox){
    checkbox.disabled = true;

    $.post('/Todo/MarkDone', {id:checkbox.name}, function(){
        var row = checkbox.parentElement.parentElement;
        $(row).addClass('done');
    });
}