using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AspNetCoreTodo.Models;

namespace AspNetCoreTodo.Services
{
    public class FakeTodoItemService : ITodoItemService
    {
        public Task<IEnumerable<TodoItem>> GetIncompleteItemsAsync(ApplicationUser user)
        {
            IEnumerable<TodoItem> items = new TodoItem[]
            {
                new TodoItem{Title="Learn asp.net core", DueAt=DateTimeOffset.Now.AddDays(1)}, 
                new TodoItem{Title="Build awesome apps", DueAt=DateTimeOffset.Now.AddDays(2)},                 
            };

            return Task.FromResult(items);
        }

        public Task<bool> AddItemAsync(NewTodoItem newItem, ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> MarkDone(Guid id, ApplicationUser user)
        {
            throw new NotImplementedException();
        }
    }
}