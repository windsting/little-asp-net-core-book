using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreTodo.Data;
using AspNetCoreTodo.Models;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreTodo.Services
{
    public class TodoItemService : ITodoItemService
    {
        private readonly ApplicationDbContext context;
        public TodoItemService(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<TodoItem>> GetIncompleteItemsAsync(ApplicationUser user)
        {
            var items = await context.Items
                .Where(x => x.IsDone == false &&
                            x.OwnerId == user.Id)
                .ToArrayAsync();

            return items;
        }

        public async Task<bool> AddItemAsync(NewTodoItem newItem, ApplicationUser user)
        {
            var entity = new TodoItem
            {
                Id = Guid.NewGuid(),
                OwnerId = user.Id,
                IsDone = false,
                Title = newItem.Title,
                DueAt = DateTimeOffset.Now.AddDays(3)
            };

            context.Items.Add(entity);
            var saveResult = await context.SaveChangesAsync();
            return saveResult == 1;
        }

        public async Task<bool> MarkDone(Guid id, ApplicationUser user)
        {
            var item = await context.Items
                .Where(x => x.Id == id && x.OwnerId == user.Id)
                .FirstOrDefaultAsync();

            if (item == null)
                return false;

            item.IsDone = true;

            var saveResult = await context.SaveChangesAsync();
            return saveResult == 1;
        }
    }
}