using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AspNetCoreTodo.Services;
using AspNetCoreTodo.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace AspNetCoreTodo.Controllers
{
    [Authorize]
    public class TodoController : Controller
    {
        private readonly ITodoItemService todoItemService;
        private readonly UserManager<ApplicationUser> userManager;

        public TodoController(ITodoItemService todoItemService,
                              UserManager<ApplicationUser> userManager)
        {
            this.todoItemService = todoItemService;
            this.userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var curUser = await userManager.GetUserAsync(User);
            if (curUser == null)
                return Challenge();

            var todoItems = await todoItemService.GetIncompleteItemsAsync(curUser);

            var model = new TodoViewModel()
            {
                Items = todoItems
            };

            return View(model);
        }

        public async Task<IActionResult> AddItem(NewTodoItem newItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var curUser = await userManager.GetUserAsync(User);
            if (curUser == null)
                return Unauthorized();

            var successful = await todoItemService.AddItemAsync(newItem, curUser);
            if (!successful)
            {
                return BadRequest(new { error = "Could not add item" });
            }

            return Ok();
        }

        public async Task<IActionResult> MarkDone(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var curUser = await userManager.GetUserAsync(User);
            if (curUser == null)
                return Unauthorized();

            var successful = await todoItemService.MarkDone(id, curUser);
            if (!successful)
                return BadRequest();

            return Ok();
        }
    }
}